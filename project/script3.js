
console.log("script connected");
window.onload = setup;
let lastIndex = 1;
let lastTime = (new Date()).toLocaleString();

//Create div, default look is ugly. Too lazy to style with less
function createDiv(message) {
    let msgdate = new Date(message.time);
    return `<div class="message" idx="${message.idx}">
       <div class="time">${msgdate.toLocaleTimeString()}</div>
       <div class="name">${message.usr}</div>
       <div class="message">${message.message}</div><br>
    </div>
    `
}

//helper for retreiving messages
function messageRequest(url, data) {
    return $.ajax({
        method: "POST",
        url: url,
        data: data
    }).done(function (msg) {  //function to populate the scroll window with new messages
        let messageArray = JSON.parse(msg);  //parse message
        //append messages
        if (messageArray.length != 0) {
            let messages = $(".messages");
            for (let msg of messageArray) {
                messages.append($(createDiv(msg)));
            }
            lastTime = messageArray[messageArray.length - 1].time;
            messages.scrollTop(messages[0].scrollHeight); //scroll to bottom of window
            console.log("Messages inserted:", messageArray.length); //used for debugging

        }
    }).always(function () {
        setTimeout(updateMessages,1000)  //use set time out, to make sure only 1 ajax request in flight at any given time.
                                         //Not strictly necessary, could have just used setInterval for simple version.
    })
}
//get initial 10 messages within the last  hour(could be less than 10)
function initialMessages() {
    return messageRequest("api/last10.php", {});
}
//update messages relative index of last message received
function updateMessages() {
    return messageRequest("api/latest.php", { time : lastTime });
}

function setupUIHandlers(){
    $("#sendButton").click(function (event) {
        $.ajax({
            method: "POST",
            url: "api/insert.php",
            data: {
                user: $("#username").val(),
                message: $("#message").val()
            }
        }).done(function (msg) {
            console.log(msg); //debugging
        }).always(function () {
                console.log("request finished");  //debugging
            })
    });
}

//Setup page
function setup() {
    initialMessages();
    setupUIHandlers();
}
