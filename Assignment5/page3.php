<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Pokemon</title>
  </head>
  <?php
      //Setup
      include "sqlhelper.php";

      //database stuff first
         //SQL  portion
         $user = 'root';
         $pwd = 'root';
         $server = 'localhost';
         $dbname = 'pokedex';

         $conn = new mysqli($server, $user, $pwd, $dbname);
         $pokemon = mysqli_real_escape_string($conn, $_GET["pokemon3"]); //prevent SQL injection

         $result = $conn->query("call type(\"$pokemon\")");  //Need to escape quotes inside quotes
         clearConnection($conn);
         //then generate the table
?>


  <body>
      <h1>Pokemon</h1>
      <p><a href="pokemon.php">Home Page</a></p>
      <?php
        echo "TYPE :" ."$pokemon";
        if($result){

          $table = $result->fetch_all(MYSQLI_ASSOC); //returns a table or rows. Rows are associative arrays

          foreach( $table as $rows){
            echo "<p>";
            echo $rows["NAT_NO"];

            echo " , "  .$rows["NAME"];

            echo " , "  .$rows["TYPE"];

            if($rows["TYPE2"]==NULL)
            {
              echo " " ."NULL";
            }
            echo "  "  .$rows["TYPE2"];

            echo " , "  .$rows["HP"];
            echo " , "  .$rows["ATK"];
            echo " , "  .$rows["DEF"];
            echo " , "  .$rows["SAT"];
            echo " , "  .$rows["SDF"];
            echo " , "  .$rows["SPD"];
            echo " , "  .$rows["BST"];
            echo "</p>";


          }

       }
        else {
          echo "error";
       }

       ?>
  </body>
</html>
