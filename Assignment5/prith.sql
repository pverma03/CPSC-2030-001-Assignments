DELIMITER //
CREATE PROCEDURE pokelist ()
BEGIN
  SELECT *
  From pokemon
  GROUP BY pokemon.NAME
  ORDER BY pokemon.NAT_NO;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE pokemon1
(IN str text)
BEGIN
  SELECT *
  From pokemon AS p
  INNER JOIN weaka ON weaka.TYPE = p.TYPE
  INNER JOIN resistant ON resistant.TYPE = p.TYPE
   INNER JOIN stronga ON stronga.TYPE=p.TYPE
    INNER JOIN vulnerable ON vulnerable.TYPE = p.TYPE
    WHERE p.NAME = str
    GROUP BY p.NAME;
END //
DELIMITER ;



DELIMITER //
CREATE PROCEDURE pokemon2
(IN str text)
BEGIN
  SELECT *
  From pokemon AS p
  INNER JOIN weaka ON weaka.TYPE = p.TYPE2
  INNER JOIN resistant ON resistant.TYPE = p.TYPE2
   INNER JOIN stronga ON stronga.TYPE=p.TYPE2
    INNER JOIN vulnerable ON vulnerable.TYPE = p.TYPE2
    WHERE p.NAME = str
    GROUP BY p.NAME;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE type
(IN str text)
BEGIN
  SELECT *
  From pokemon AS p
  WHERE p.TYPE = str OR p.TYPE2 = str
  GROUP BY p.NAME;
END //
DELIMITER ;
