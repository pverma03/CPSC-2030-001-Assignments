<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Pokemon</title>
  </head>
  <?php
      //Setup
      include "sqlhelper.php";

      //database stuff first
         //SQL  portion
         $user = 'root';
         $pwd = 'root';
         $server = 'localhost';
         $dbname = 'pokedex';

         $conn = new mysqli($server, $user, $pwd, $dbname);
         $pokemon = mysqli_real_escape_string($conn, $_GET["pokemon"]); //prevent SQL injection
         $conn2 = new mysqli($server, $user, $pwd, $dbname);
         $result = $conn->query("call pokemon1(\"$pokemon\")");
        $result2 =  $conn2->query("call pokemon2(\"$pokemon\")");
         clearConnection($conn);
         clearConnection($conn2);
?>


  <body>
      <h1>Pokemon</h1>
      <p><a href="pokemon.php">Home Page</a></p>
      <?php
        echo "NAME :" ."$pokemon";
        if($result ){

          $table = $result->fetch_all(MYSQLI_ASSOC); //returns a table or rows. Rows are associative arrays
          $table2 = $result2->fetch_all(MYSQLI_ASSOC);
          foreach( $table as $row ){


            echo "<p>";
            echo "NAT_NO :" . $row["NAT_NO"] ."</br>";

            echo "TYPE :" ." " .$row["TYPE"] ."</br>";
            echo "HP :" ." "  .$row["HP"] ."</br>";
            echo "ATK :" ." " .$row["ATK"] ."</br>";
            echo "DEF :" ." " .$row["DEF"] ."</br>";
            echo "SAT :" ." " .$row["SAT"] ."</br>";
            echo "SDF :" ." " .$row["SDF"] ."</br>";
            echo "SPD :" ." " .$row["SPD"] ."</br>";
            echo "BST :" ." " .$row["BST"] ."</br>";
            echo "VULNERABLETO :" ." " .$row["VULNERABLETO"] ."</br>";
            echo "RESISTANTTO :" ." " .$row["RESISTANTTO"] ."</br>";
            echo "WEAKAGAINST :" ." " .$row["WEAKAGAINST"] ."</br>";
            echo "STRONGAGAINST :" ." " .$row["STRONGAGAINST"] ."</br>";


          }
          echo "FOR TYPE2 :" . $row["TYPE2"] . "</br>";
      
          foreach ($table2 as $row2) {

              echo "VULNERABLETO :" ." " .$row2["VULNERABLETO"] ."</br>";
              echo "RESISTANTTO :" ." " .$row2["RESISTANTTO"] ."</br>";
              echo "WEAKAGAINST :" ." " .$row2["WEAKAGAINST"] ."</br>";
              echo "STRONGAGAINST :" ." " .$row2["STRONGAGAINST"] ."</br>";

          }

            echo "</p>";
          }

        else {
          echo "error";
       }

       ?>
  </body>
</html>
