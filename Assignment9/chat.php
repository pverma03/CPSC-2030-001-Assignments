<!DOCTYPE html>
<html lang="en" dir="ltr">
	<head>
		<meta charset="utf-8">

		<title>Chat Room</title>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

			<script src="chatting.js" defer></script>
	</head>
	<body>
		<div id="chatbox">

		</div>
			<form name = "info">
				Enter your name: <input type="text" name="name">
				Enter your message: <input type="text" name="message">
				<button type="button" onclick="messageSend()">Reply</button>
			</form>

	</body>
</html>
