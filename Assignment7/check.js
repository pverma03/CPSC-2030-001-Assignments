let index;
let i=0;
function Valkyrie(name,side,unit,rank,role,description,img)
{
	this.name = name;
	this.side = side;
	this.unit = unit;
	this.rank = rank;
	this.role = role;
	this.description = description;
	this.img = img;

}
let riley = new Valkyrie("Riley Miller","Edinburgh Army","Federate Joint Ops","Second Lieutenant","Artillery Advisor","Born in the Gallian city of Hafen, this brilliant inventor was assigned to Squad E after researching ragnite technology in the United States of Vinland. She appears to share some history with Claude, although the memories seem to be traumatic ones. ","1.png");

let raz = new Valkyrie("Raz","Edinburgh Army","Ranger Corps, Squad E","Sergeant","Fireteam Leader","Born in the Gallian city of Hafen, this foul-mouthed Darcsen worked his way up from the slums to become a capable soldier. Though foul-mouthed and reckless, his athleticism and combat prowess is top-notch... And according to him, he's invincible.","2.png");

let roland = new Valkyrie("Roland Morgen","Edinburgh Navy","Centurion, Cygnus Fleet","Ship's Captain","Cruiser Commander","Born in the United Kingdom of Edinburgh, this naval officer commands a state-of-the-art snow cruiser named the Centurion. For a ship's captain, his disposition is surprisingly mild-mannered. As such, he never loses his composure, even in the direst of straits. ","3.png");

let ragnarok = new Valkyrie("Ragnarok","Edinburgh Army","Squad E","K-9 Unit","Mascot","Once a stray, this good good boy is lovingly referred to as Rags.As a K-9 unit, he's a brave and intelligent rescue dog who's always willing to lend a helping paw. When the going gets tough, the tough get ruff. ","4.png");

let karen = new Valkyrie("Karen Stuart","Edinburgh Army","Squad E","Corporal","Combat EMT","Born as the eldest daughter of a large family, this unflappable field medic is an expert at administering first aid in the heat of battle. Although she had plans to attend medical school, she instead enlisted in her nation's military to support her growing household. ","5.png");

let miles = new Valkyrie("Miles Arbeck","Edinburgh Army"," Ranger Corps, Squad E","Sergeant","Tank Operator","Born in the United Kingdom of Edinburgh, this excitable driver was Claude Wallace's partner in tank training, and was delighted to be assigned to Squad E. He's taken up photography as a hobby, and is constantly taking snapshots whenever on standby.","6.png");

let ronald = new Valkyrie("Ronald Albee","Edinburgh Army","Ranger Corps","Second Lietuent","Tank Operator","Born in the United Kingdom of Edinburgh, this stern driver was Minerva Victor's underclassman at the military academy. Upon being assigned to Squad F, he swore an oath of fealty to Lt. Victor, and takes great satisfaction in upholding her chivalric code.","7.png");

let andre = new Valkyrie("Andre","Edinburgh Navy","Centurion, Cygnus Fleet","Lieutenant","Chief Engineer","As the Centurion's crewmember responsible for maintenance and repairs, this EW1 veteran runs a tight ship. With a fondness for harsh language and hard liquor, André's overworked yet loyal mechanics fondly refer to him as the crew's grumpy old uncle.","8.png");

let marie = new Valkyrie("Marie Benett","Edinburgh Navy","Centurion, Cygnus Fleet","Petty officer","Chief of operations","As the Centurion's crewmember responsible for overseeing daily operations, this gentle and supportive EWI veteran even takes daily tasks like cooking and cleaning upon herself. She never forgets to wear a smile. Her age is undisclosed, even in her personnel files. ","9.png");

let brian = new Valkyrie("Brian Haddock","Edinburgh Navy","Centurion","Lieutenant","Cheif Navigator","As the Centurion's crewmember responsible for navigation, this straight-laced navy man is considered by his peers to be the ship's de facto first mate. He holds Cpt. Morgen in very high regard. Enjoys gardening, and keeps a collection of potted plants in his quarters.","10.png");



let character = [riley,raz,ronald,ragnarok,karen,miles,ronald,andre,marie,brian];

function display()
{
	document.getElementById('name').innerHTML = character[index].name;
	document.getElementById('side').innerHTML = character[index].side;
	document.getElementById('rank').innerHTML = character[index].rank;
	document.getElementById('unit').innerHTML = character[index].unit;
	document.getElementById('role').innerHTML = character[index].role;
	document.getElementById('description').innerHTML = character[index].description;
	document.getElementById('image').innerHTML = "<img src ="+character[index].img+">";

}
function checkID(event)
{
	 index = event.target.id;
}
for(let char of character){
	let valk = document.createElement("DIV");
	valk.className = 'char';
	valk.id = i;
	// valk.setAttribute("");
	valk.setAttribute("onmouseover","checkID(event),display()");
	let valktext = document.createTextNode(char.name);
	valk.appendChild(valktext);
	document.getElementById('Battalion').appendChild(valk);
	i++;
}
